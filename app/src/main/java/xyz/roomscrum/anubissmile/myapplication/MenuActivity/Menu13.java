package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import xyz.roomscrum.anubissmile.myapplication.R;

public class Menu13 extends AppCompatActivity {

    private Button buzzer, bell;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu13);

        buzzer = (Button)findViewById(R.id.buzzer);
        bell = (Button)findViewById(R.id.bell);

        buzzer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*PLAYING BUZZER SOUND.*/
                MediaPlayer mp = MediaPlayer.create(Menu13.this, R.raw.wrong_buzzer);
                mp.start();
            }
        });

        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*PLAYING BELL SOUND.*/
                MediaPlayer mp = MediaPlayer.create(Menu13.this, R.raw.win_bell);
                mp.start();
            }
        });
    }
}
