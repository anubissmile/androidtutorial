package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import xyz.roomscrum.anubissmile.myapplication.R;

public class Menu2 extends AppCompatActivity {
    int a=10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu2);
        Log();
    }
    public void Log(){
        for (int i=0;i<a;i++){
            if (i<=5){
                Log.v("Result","i < 5");
                Log.d("Result","i < 5");
                Log.i("Result","i < 5");
                Log.w("Result","i < 5");
                Log.e("Result","i < 5");
            }else{
                Log.v("Result","FALSE");
                Log.d("Result","FALSE");
                Log.i("Result","FALSE");
                Log.w("Result","FALSE");
                Log.e("Result","FALSE");
            }
        }
    }

}
