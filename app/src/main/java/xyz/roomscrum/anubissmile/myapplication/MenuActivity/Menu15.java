package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import xyz.roomscrum.anubissmile.myapplication.R;
import xyz.roomscrum.anubissmile.myapplication.SqliteDB;

public class Menu15 extends AppCompatActivity {
public static ArrayList<String> MemberArray = new ArrayList<String>();
    ListView listView;
    SQLiteDatabase mDb;
    Cursor mCursor;
    SqliteDB db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu15);
        listView = (ListView)findViewById(R.id.Listview);
        setAdapter();
    }
    public void setAdapter(){
        if (Menu14.CheckClick.equals("0")){
            Log.e("Check ","0");
            db =new SqliteDB(this);
            mDb = db.getWritableDatabase();
            mCursor = mDb.rawQuery("SELECT " + db.COL_Firstname + ", "  + db.COL_Lastname
                    + " FROM " + db.TABLE_MEMBER, null);
            if (mCursor != null)
            {
                if (mCursor.moveToFirst())
                {
                    do
                    {
                        String Firstname = mCursor.getString(mCursor.getColumnIndex(db.COL_Firstname));
                        String Lastname = mCursor.getString(mCursor.getColumnIndex(db.COL_Lastname));
                        MemberArray.add("ชื่อ "+Firstname+"   นามสกุล "+Lastname);

                    } while (mCursor.moveToNext());
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, MemberArray);
                    listView.setAdapter(adapter);
                }
            }
        }else{
            Log.e("Check ","1");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, MemberArray);
            listView.setAdapter(adapter);
        }

    }

}
