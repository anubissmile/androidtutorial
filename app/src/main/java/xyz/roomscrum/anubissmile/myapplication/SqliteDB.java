package xyz.roomscrum.anubissmile.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Developer on 1/22/2017.
 */

public class SqliteDB  extends SQLiteOpenHelper {
     private static final int DATABASE_VERSION = 1;
     private static final String DATABASE_NAME = "testDB";
    public static final String TABLE_MEMBER = "DBmember";
     public  static final String COL_Firstname="Firstname";
    public  static final String COL_Lastname="Lastname";
    public SqliteDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        db.execSQL("CREATE TABLE " + TABLE_MEMBER +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " "+COL_Firstname+" TEXT(100)," +
                " "+COL_Lastname+" TEXT(100));");
        Log.d("CREATE TABLE","Successfully.");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }
    public long InsertData(String strFirstname, String strLastname) {
        // TODO Auto-generated method stub
        try {
            SQLiteDatabase db;
            db = this.getWritableDatabase();
            ContentValues Val = new ContentValues();
              Val.put(COL_Firstname, strFirstname);
             Val.put(COL_Lastname, strLastname);
            long rows = db.insert(TABLE_MEMBER, null, Val);
            db.close();
            return rows;
        } catch (Exception e) {
            return -1;
        }

    }
//    public String[] SelectData(String strMember) {
//        // TODO Auto-generated method stub
//
//        try {
//            String arrData[] = null;
//
//            SQLiteDatabase db;
//            db = this.getReadableDatabase(); // Read Data
//
//            Cursor cursor = db.query(TABLE_MEMBER, new String[] { "*" },""+COL_Firstname+"=?", new String[] {
//                            String.valueOf(strMember)}, null, null, null, null);
//
//            if(cursor != null)
//            {
//                if (cursor.moveToFirst()) {
//                    arrData = new String[cursor.getColumnCount()];
//
//                     arrData[0] = cursor.getString(0);
//                    arrData[1] = cursor.getString(1);
//                }
//            }
//            cursor.close();
//            db.close();
//            return arrData;
//
//        } catch (Exception e) {
//            return null;
//        }
//
//    }
}
