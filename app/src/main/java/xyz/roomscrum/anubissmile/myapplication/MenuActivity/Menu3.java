package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import xyz.roomscrum.anubissmile.myapplication.CustomDIalog;
import xyz.roomscrum.anubissmile.myapplication.R;

public class Menu3 extends AppCompatActivity {
Button customDialog,Dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu3);
        Dialog = (Button)findViewById(R.id.Dialog);
        customDialog = (Button)findViewById(R.id.CustomDialog);

        /*THIS IS TOAST*/
        Toast.makeText(this, "Welcome My name is Toast", Toast.LENGTH_SHORT).show();
        /*=========================================================================*/

        Dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog();
            }
        });
        customDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDIalog customdialog = new CustomDIalog(Menu3.this);
                customdialog.show();
            }
        });
    }
    public void Dialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(Menu3.this).create();
        alertDialog.setTitle("Dialog");
        alertDialog.setMessage("Hello World");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
