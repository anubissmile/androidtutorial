package xyz.roomscrum.anubissmile.myapplication.MenuActivity.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import xyz.roomscrum.anubissmile.myapplication.R;

public class PreferenceActivity extends AppCompatActivity {
    SharedPreferences sp,spq;
    String Firstname,Lastname,Sex;
    TextView Fname,ChkSex;
    private static boolean CheckSave;
    Button Clear;

    public static boolean isCheckSave() {
        return CheckSave;
    }

    public static void setCheckSave(boolean checkSave) {
        CheckSave = checkSave;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        Clear = (Button)findViewById(R.id.Clear);
        sp = getApplicationContext().getSharedPreferences("Preferences", getApplicationContext().MODE_PRIVATE);
        Firstname = sp.getString("Firstname","");
        Lastname = sp.getString("Lastname","");
        Sex = sp.getString("Sex","");
        Fname = (TextView)findViewById(R.id.FirstName);
         ChkSex = (TextView)findViewById(R.id.Sex);
        Fname.setText("ชื่อ "+Firstname+" นามสกุล "+Lastname);
        ChkSex.setText("เพศ "+Sex);

        spq = getApplicationContext().getSharedPreferences("PreferencesCheck", getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor Check2 = spq.edit();
        Check2.putString("CheckVisit","1");
        setCheckSave(true);
        Check2.commit();
        Clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sp.edit();
                editor.clear();
                editor.commit();
                Fname.setText("");
                ChkSex.setText("");
                SharedPreferences.Editor editor2 = spq.edit();
                editor2.clear();
//                editor2.putString("CheckVisit", "0");
                editor2.commit();

                setCheckSave(false);
                Toast.makeText(PreferenceActivity.this, "Clear SharedPreferences", Toast.LENGTH_SHORT).show();
             }
        });
    }
}
