package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import xyz.roomscrum.anubissmile.myapplication.R;

public class Menu7 extends AppCompatActivity {
    EditText text1,text2;
    String Savetext1,Savetext2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu7);
        text1 = (EditText)findViewById(R.id.text1);
        text2 = (EditText)findViewById(R.id.text2);
        Savetext1 = text1.getText().toString();
        Savetext2 = text2.getText().toString();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("text1", Savetext1);
        outState.putString("text2", Savetext2);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Savetext1 = savedInstanceState.getString("text1");
        Savetext2 = savedInstanceState.getString("text2");
    }
}
