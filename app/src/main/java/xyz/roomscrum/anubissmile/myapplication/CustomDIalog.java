package xyz.roomscrum.anubissmile.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Developer on 1/15/2017.
 */

public class CustomDIalog extends Dialog implements View.OnClickListener{

    public CustomDIalog(Context context) {
        super(context);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);
        Button OK = (Button)findViewById(R.id.OK);

        OK.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.OK:
                Toast.makeText(getContext(),"OK", Toast.LENGTH_LONG).show();
                dismiss();
                break;
            default:
                break;
        }
     }


}
