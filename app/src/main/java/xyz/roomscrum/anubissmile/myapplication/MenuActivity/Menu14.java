package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import xyz.roomscrum.anubissmile.myapplication.R;
import xyz.roomscrum.anubissmile.myapplication.SqliteDB;

public class Menu14 extends AppCompatActivity {
    EditText Fname,Lname;
    Button Add,Get;
    SQLiteDatabase mDb;
    Cursor mCursor;
    SqliteDB db;
    public static String CheckClick="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu14);
        Fname = (EditText) findViewById(R.id.FirstName);
        Lname = (EditText)findViewById(R.id.Lastname);
        Add = (Button)findViewById(R.id.Add);
        Get = (Button)findViewById(R.id.Get);
             db =new SqliteDB(this);
        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 long checkDB = db.InsertData(Fname.getText().toString(), Lname.getText().toString());
                if(checkDB > 0)
                {
                    Toast.makeText(Menu14.this,"Insert Data Successfully",Toast.LENGTH_LONG).show();
                    mDb = db.getWritableDatabase();
                    mCursor = mDb.rawQuery("SELECT " + db.COL_Firstname + ", "  + db.COL_Lastname
                            + " FROM " + db.TABLE_MEMBER, null);
                    if (mCursor != null)
                    {
                        if (mCursor.moveToFirst())
                        {
                            CheckClick="1";
                            do
                            {
                                String Firstname = mCursor.getString(mCursor.getColumnIndex(db.COL_Firstname));
                                String Lastname = mCursor.getString(mCursor.getColumnIndex(db.COL_Lastname));
                                Menu15.MemberArray.add("ชื่อ "+Firstname+"   นามสกุล "+Lastname);
                             } while (mCursor.moveToNext());
                        }
                    }
                }
                else
                {
                    Toast.makeText(Menu14.this,"Insert Data Failed.",
                            Toast.LENGTH_LONG).show();
                }

            }
        });
        Get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDb = db.getWritableDatabase();
                mCursor = mDb.rawQuery("SELECT " + db.COL_Firstname + ", "  + db.COL_Lastname
                        + " FROM " + db.TABLE_MEMBER, null);
                if (mCursor != null)
                {
                    if (mCursor.moveToFirst())
                    {
                        do
                        {
                            String Firstname = mCursor.getString(mCursor.getColumnIndex(db.COL_Firstname));
                            String Lastname = mCursor.getString(mCursor.getColumnIndex(db.COL_Lastname));
                            Log.e("Getdatabase ","Firstname "+Firstname+" Lastname "+Lastname);
                            Toast.makeText(Menu14.this, "ชื่อ "+Firstname+" นามสกุล "+Lastname, Toast.LENGTH_SHORT).show();
                        } while (mCursor.moveToNext());
                    }
                }
            }
        });
    }

}
