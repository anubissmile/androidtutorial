package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import xyz.roomscrum.anubissmile.myapplication.R;

public class Menu5 extends AppCompatActivity {
    RadioGroup group;
    RadioButton radioButton;
    Button Click;
    TextView Text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu5);
        group = (RadioGroup)findViewById(R.id.radio_group);
        Click = (Button)findViewById(R.id.Click);
        Text = (TextView)findViewById(R.id.Text);
        Click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = group.getCheckedRadioButtonId();
                radioButton = (RadioButton)findViewById(selectedId);
                 Text.setText(radioButton.getText().toString());
            }
        });
    }
}
