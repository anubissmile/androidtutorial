package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.Calendar;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Activity.IntentActivity;
import xyz.roomscrum.anubissmile.myapplication.R;
public class Menu6 extends AppCompatActivity {
EditText Name,birth;
    Button Click;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu6);
        Name = (EditText)findViewById(R.id.Name);
        birth = (EditText)findViewById(R.id.Birth);
        Click = (Button)findViewById(R.id.Click);
        Click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Name.getText().toString()!=null&&!Name.getText().toString().equals("")
                        &&birth.getText().toString()!=null&&!birth.getText().toString().equals(""))
                {
                    IntentData();
                }else if (Name.getText().toString()==null||Name.getText().toString().equals("")){
                    Name.setError("กรุณาใส่ชื่อ");
                }else if(birth.getText().toString()==null||birth.getText().toString().equals("")){
                    birth.setError("กรุณาใส พ.ศ.");
                }else{
                    Name.setError("กรุณาใส่ชื่อ");
                    birth.setError("กรุณาใส พ.ศ.");
                }
            }
        });
    }
    public void IntentData(){
        String sumBirth = birth.getText().toString();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int ConvertYear = year+543;
        if (ConvertYear>Integer.parseInt(sumBirth)) {
            int SumAge = ConvertYear - Integer.parseInt(sumBirth);
            Intent intent = new Intent(Menu6.this,IntentActivity.class);
            intent.putExtra("Name", Name.getText().toString());
            intent.putExtra("Birth", String.valueOf(SumAge));
            startActivity(intent);
        }else{
            Toast.makeText(Menu6.this,"พ.ศ. เกิดต้องน้อยกว่า พ.ศ. ปัจจุบัน",Toast.LENGTH_LONG).show();
        }
    }
}
