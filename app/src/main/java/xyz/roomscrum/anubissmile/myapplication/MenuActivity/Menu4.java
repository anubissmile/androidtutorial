package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import xyz.roomscrum.anubissmile.myapplication.HomeActivity;
import xyz.roomscrum.anubissmile.myapplication.R;
public class Menu4 extends AppCompatActivity {
public static int check;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu4);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
         getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Intent intent = new Intent(Menu4.this, HomeActivity.class);
                 startActivity(intent);
                return true;
            case R.id.item2:
                finish();
                return true;
            case R.id.item3:
                Intent intent2 = new Intent(Menu4.this, HomeActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                check =1;
                startActivity(intent2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
