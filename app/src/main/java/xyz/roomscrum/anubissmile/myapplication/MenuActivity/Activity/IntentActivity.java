package xyz.roomscrum.anubissmile.myapplication.MenuActivity.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Text;

import xyz.roomscrum.anubissmile.myapplication.R;

public class IntentActivity extends AppCompatActivity {
    TextView Name,Birth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
        Name = (TextView)findViewById(R.id.Name);
        Birth = (TextView)findViewById(R.id.Birth);
        Bundle bundle= getIntent().getExtras();
         Name.setText("ชื่อ "+bundle.getString("Name"));
         Birth.setText("อายุ "+bundle.getString("Birth")+" ปี");
    }
}
