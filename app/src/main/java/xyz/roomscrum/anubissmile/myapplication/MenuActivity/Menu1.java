package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import xyz.roomscrum.anubissmile.myapplication.R;


public class Menu1 extends AppCompatActivity {
    TextView Text;
    Button Click;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu1);
        Text = (TextView)findViewById(R.id.Text);
        Click = (Button)findViewById(R.id.Click);
        Click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Text.setText("Hello World");
            }
        });
    }
}
