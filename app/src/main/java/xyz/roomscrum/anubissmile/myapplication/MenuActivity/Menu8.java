package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Activity.PreferenceActivity;
import xyz.roomscrum.anubissmile.myapplication.R;

public class Menu8 extends AppCompatActivity {
    EditText firstname,lastname;
    RadioGroup group;
    RadioButton radioButton;
    Button Save,VisitProfile;
    Boolean chkContain;
    public String Check;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu8);
        group = (RadioGroup)findViewById(R.id.radio_group);
        Save = (Button)findViewById(R.id.Save);
        VisitProfile = (Button)findViewById(R.id.VisitProfile);
        firstname = (EditText)findViewById(R.id.FirstName);
        lastname = (EditText)findViewById(R.id.Lastname);

        chkVisit();
        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences();
                Toast.makeText(Menu8.this, "Save SharedPreferences", Toast.LENGTH_SHORT).show();
            }
        });

            VisitProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view){
//                    if (Check.equals("1") && PreferenceActivity.isCheckSave()){
                    chkVisit();
                    if(chkContain && Check.equals("1")){
                        Intent intent =new Intent(Menu8.this,PreferenceActivity.class);
                        startActivity(intent);
                    }else{
//                        Intent intent = new Intent(Menu8.this,PreferenceActivity.class);
//                        startActivity(intent);
                        Toast.makeText(Menu8.this, "ไม่พบข้อมูล", Toast.LENGTH_SHORT).show();
                    }
                }
            });
    }

    public void chkVisit(){
        SharedPreferences sp = getApplicationContext().getSharedPreferences("PreferencesCheck", getApplicationContext().MODE_PRIVATE);
        Check = sp.getString("CheckVisit","");
        chkContain = sp.contains("CheckVisit");
        Log.e("CheckVisit",Check);
        Log.e("chkContain", String.valueOf(chkContain));
    }

    public void Preferences(){
        int selectedId = group.getCheckedRadioButtonId();
        radioButton = (RadioButton)findViewById(selectedId);
        SharedPreferences spq = getApplicationContext().getSharedPreferences("Preferences", getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = spq.edit();
        editor.putString("Firstname",firstname.getText().toString());
        editor.putString("Lastname",lastname.getText().toString());
        editor.putString("Sex", radioButton.getText().toString());
        editor.commit();
        Intent intent = new Intent(Menu8.this, PreferenceActivity.class);
        startActivity(intent);
      }
}
