package xyz.roomscrum.anubissmile.myapplication.MenuActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import xyz.roomscrum.anubissmile.myapplication.MainActivity;
import xyz.roomscrum.anubissmile.myapplication.R;

public class Menu16 extends AppCompatActivity {
    private MediaPlayer mp = null;

    private String kVoiceRssServer = "http://api.voicerss.org";
    private String kVoiceRSSAppKey = "128f5395208441ef9f027499f47d9838"; /*put your AppKey here*/

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu16);
    }

    public void btnSpeakNowOnClick(View v)
    {
        EditText txtSentence = (EditText) findViewById(R.id.txtSentence);

        final String text = txtSentence.getText().toString();

        try
        {
            final MediaPlayer.OnPreparedListener onPreparedListener = new MediaPlayer.OnPreparedListener()
            {
                @Override
                public void onPrepared(MediaPlayer mp)
                {
                    mp.setVolume(1, 1);
                    mp.start();
                }
            };

            final MediaPlayer.OnErrorListener onErrorListener = new MediaPlayer.OnErrorListener()
            {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra)
                {
                    return false;
                }
            };

            final MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mp)
                {
                    mp.release();
                    mp = null;
                }
            };

            String url = buildSpeechUrl(text, "en-us");

            AudioManager audioManager = (AudioManager)getSystemService(MainActivity.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);

            try
            {
                if (mp != null)
                    mp.release();

                mp = new MediaPlayer();
                mp.setDataSource(url);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.setOnErrorListener(onErrorListener);
                mp.setOnCompletionListener(onCompletionListener);
                mp.setOnPreparedListener(onPreparedListener);
                mp.prepareAsync();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /** Build speech URL. */
    private String buildSpeechUrl(String words, String language)
    {
        String url = "";

        url = kVoiceRssServer + "/?key=" + kVoiceRSSAppKey + "&t=text&hl=" + language + "&src=" + words;

        return url;
    }
}
