package xyz.roomscrum.anubissmile.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu1;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu10;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu11;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu12;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu13;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu14;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu15;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu16;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu2;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu3;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu4;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu5;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu6;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu7;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu8;
import xyz.roomscrum.anubissmile.myapplication.MenuActivity.Menu9;

public class HomeActivity extends AppCompatActivity {
    TextView menu1,menu2,menu3,menu4,menu5,menu6,menu7,menu8,menu9,menu10,menu11,menu12
            ,menu13,menu14,menu15,menu16;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        menu1 = (TextView)findViewById(R.id.Menu1);
        menu2 = (TextView)findViewById(R.id.Menu2);
        menu3 = (TextView)findViewById(R.id.Menu3);
        menu4 = (TextView)findViewById(R.id.Menu4);
        menu5 = (TextView)findViewById(R.id.Menu5);
        menu6 = (TextView)findViewById(R.id.Menu6);
        menu7 = (TextView)findViewById(R.id.Menu7);
        menu8 = (TextView)findViewById(R.id.Menu8);
        menu9 = (TextView)findViewById(R.id.Menu9);
        menu10 = (TextView)findViewById(R.id.Menu10);
        menu11 = (TextView)findViewById(R.id.Menu11);
        menu12 = (TextView)findViewById(R.id.Menu12);
        menu13 = (TextView)findViewById(R.id.Menu13);
        menu14 = (TextView)findViewById(R.id.Menu14);
        menu15 = (TextView)findViewById(R.id.Menu15);
        menu16 = (TextView)findViewById(R.id.Menu16);
        if (Menu4.check==1){
            Menu4.check=0;
            finish();
        }
        menu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu1.class);
                startActivity(intent);
            }
        });
        menu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu2.class);
                startActivity(intent);
            }
        });
        menu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu3.class);
                startActivity(intent);
            }
        });
        menu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu4.class);
                startActivity(intent);
            }
        });
        menu5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu5.class);
                startActivity(intent);
            }
        });
        menu6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu6.class);
                startActivity(intent);
            }
        });
        menu7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu7.class);
                startActivity(intent);
            }
        });
        menu8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu8.class);
                startActivity(intent);
            }
        });
        menu9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu9.class);
                startActivity(intent);
            }
        });
        menu10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu10.class);
                startActivity(intent);
            }
        });
        menu11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu11.class);
                startActivity(intent);
            }
        });
        menu12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu12.class);
                startActivity(intent);
            }
        });
        menu13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu13.class);
                startActivity(intent);
            }
        });
        menu14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu14.class);
                startActivity(intent);
            }
        });
        menu15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu15.class);
                startActivity(intent);
            }
        });
        menu16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,Menu16.class);
                startActivity(intent);
            }
        });


    }


}
