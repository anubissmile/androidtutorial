package xyz.roomscrum.anubissmile.myapplication;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public  int a=0;
    Button Dialog,customDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /*TOAST*/
        makeToast("Hello DEV");


        chkLog();

        Dialog = (Button)findViewById(R.id.Dialog);
        customDialog = (Button)findViewById(R.id.CustomDialog);
        CheckLog();
        Dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog();
            }
        });
        customDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDIalog customdialog = new CustomDIalog(MainActivity.this);
                customdialog.show();
            }
        });
    }
    public void CheckLog(){
        if(a==0){
            Toast.makeText(this, "TRUE", Toast.LENGTH_SHORT).show();
            Log.v("Result","TRUE");
            Log.d("Result","TRUE");
            Log.i("Result","TRUE");
            Log.w("Result","TRUE");
            Log.e("Result","TRUE");
        }else{
            Toast.makeText(this, "False", Toast.LENGTH_SHORT).show();
            Log.v("Result","FALSE");
            Log.d("Result","FALSE");
            Log.i("Result","FALSE");
            Log.w("Result","FALSE");
            Log.e("Result","FALSE");
        }
    }
    public void Dialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Dialog");
        alertDialog.setMessage("Hello World");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }



    protected  void makeToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    protected void chkLog(){
        Log.v("Verbose", "msgVerbose");
        Log.d("Debug", "msgDebug");
        Log.i("Info", "msgInfo");
        Log.w("Warning", "msgWarning");
        Log.e("Info", "msgInfo");
    }

    public void SendData(){

    }
}
